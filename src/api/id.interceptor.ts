import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {State} from './api.reducer';

@Injectable()
export class IdInterceptor implements HttpInterceptor {
    id: string;

    constructor(private store: Store<State>) {
        this.store.select(state => state.configure.id).subscribe(value => {
            this.id = value;
        });
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.id) {
            const changedReq = req.clone({headers: req.headers.set('X-REQUEST-ID', this.id)});
            return next.handle(changedReq);
        }
        return next.handle(req);
    }
}
