import {Actions, CONFIGURE_FAIL, CONFIGURE_SUCCESS, ConfigureFailAction, ConfigureSuccessAction} from './configure.action';

export interface ConfigureState {
    loaded: boolean;
    server: {
        list: string[]
        path: string
        use: number;
        full: string;
    };
    cache: {
        enable: boolean;
        ttl?: number;
        exception?: string[]
    };
    version?: {
        release: string;
        time: Date
    };
    id: string | null;
}

const initialState: ConfigureState = {
    loaded: false,
    server: {
        list: [
            'http://localhost:3000',
            'http://127.0.0.1:3000'
        ],
        path: '',
        use: 0,
        full: 'http://localhost:3000'
    },
    cache: {
        enable: false
    },
    id: null

};

export function reducer(state: ConfigureState = initialState, action: Actions): ConfigureState {
    switch (action.type) {
        case CONFIGURE_SUCCESS: {
            if (!(action instanceof ConfigureSuccessAction)) {
                throw new Error();
            }
            return {...state, ...action.payload, loaded: true};
        }
        case CONFIGURE_FAIL: {
            if (!(action instanceof ConfigureFailAction)) {
                throw new Error();
            }
            return state;
        }
        default: {
            return state;
        }
    }
}
