import {Action} from '@ngrx/store';
import {HttpErrorResponse} from '@angular/common/http';
import {ConfigureState} from './configure.reducer';

export const CONFIGURE_SUCCESS = '[Configure] Success';
export const CONFIGURE_FAIL = '[Configure] Fail';

export class ConfigureSuccessAction implements Action {
    readonly type = CONFIGURE_SUCCESS;

    constructor(public payload: ConfigureState) {
        console.time(CONFIGURE_SUCCESS);
    }
}

export class ConfigureFailAction implements Action {
    readonly type = CONFIGURE_FAIL;

    constructor(public payload: HttpErrorResponse) {
    }
}


export type Actions = ConfigureSuccessAction | ConfigureFailAction;
