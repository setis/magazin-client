import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {ConfigureState} from './configure.reducer';
import {State} from '../api.reducer';
import {LoadingFailAction, LoadingSendAction, LoadingSuccessAction} from '../loading/loading.action';
import {ConfigureFailAction, ConfigureSuccessAction} from './configure.action';

@Injectable()
export class ConfigureService {
    cfg: ConfigureState;

    constructor(private http: HttpClient, private store: Store<State>) {
        this.store.select(state => state.configure).subscribe(value => {
            this.cfg = value;
        });
    }

    load() {
        this.store.dispatch(new LoadingSendAction('configure'));
        this.http.get<ConfigureState>(`/api.json`)
            .subscribe(
                value => {
                    this.store.dispatch(new LoadingSuccessAction('configure'));
                    this.store.dispatch(new ConfigureSuccessAction(value));
                },
                error => {
                    this.store.dispatch(new LoadingFailAction('configure', error));
                    this.store.dispatch(new ConfigureFailAction(error));
                });
    }

}
