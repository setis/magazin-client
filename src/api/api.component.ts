import {Component, OnInit} from '@angular/core';
import {ConfigureService} from './configure/configure.service';
import {State} from './api.reducer';
import {Store} from '@ngrx/store';

@Component({
    selector: 'app-root',
    templateUrl: './api.component.html',
    providers: [
        ConfigureService
    ]
})
export class ApiComponent implements OnInit {
    constructor(private configure: ConfigureService, private store: Store<State>) {
    }

    ngOnInit() {
        this.configure.load();
    }
}
