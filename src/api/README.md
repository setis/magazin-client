# Redux Name

###Status:

1. LOADING (загрузка) payload:boolean 
   1. false загрузка завершенна
   2. true идет загрузка
2. SUCCESS (успех)
3. FAILURE (провал)
4. LOADED (загруженны данные) payload:boolean 
   1. true данные загруженны
   2. false данные не загруженны

###Action:

1. LOAD (получение)
2. CREATE (создание)
3. UPDATE (обновление)
4. REMOVE (удаление)

```typescript
interface Registry<E>{
    [name:string]:{
     	try:number;   
        loading:boolean;
        fail:false|E
        time:Date
        loaded:boolean;
    }
}
interface Configure<T>{
   server: {
        list: string[];
        path: string;
        use: number;
        full:string;
    };
    cache: {
        enable: boolean;
        ttl: number;
        exception:string[];
    };
    version:{
        release:string;
        time:Date;
    };
    id: string;
    token: string | null;
}
interface CacheList{
    [path:string]:{
        etag:string;
        lastMod:Date;
        time:Date;
    }
}
interface CacheStore{
    [path:string]:any;
}
```

