import {CategoryUniqNameDirective} from './uniq.name.directive';
import {CategoryService} from './category.service';
import {HttpClient} from '@angular/common/http';

describe('CategoryUniqNameDirective', () => {
    it('should create an instance', () => {
        const directive = new CategoryUniqNameDirective(new CategoryService(new HttpClient()));
        expect(directive).toBeTruthy();
    });
});
