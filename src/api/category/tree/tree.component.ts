import {Component, OnInit} from '@angular/core';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {CategoryService} from '../category.service';
import {TreeItem} from 'ngx-treeview/src/treeview-item';
import {CategoryTree} from '../category.models';
import {Store} from '@ngrx/store';
import {State} from '../../api.reducer';

function convert(data: CategoryTree[]): TreeItem[] {
    return data.map(value => {
        return {
            text: value.name,
            value: value.id,
            children: convert(value.children)
        };
    });
}

@Component({
    selector: 'app-tree',
    templateUrl: './tree.component.html',
    styleUrls: ['./tree.component.scss'],
    providers: [
        CategoryService
    ]
})


export class CategoryTreeComponent implements OnInit {

    items: TreeviewItem[];
    values: number[];
    config: TreeviewConfig;

    constructor(private service: CategoryService, private store: Store<State>) {
        this.config = TreeviewConfig.create({
            hasAllCheckBox: true,
            hasFilter: true,
            hasCollapseExpand: true,
            decoupleChildFromParent: false,
            maxHeight: 400
        });
        this.items = [];
        this.values = [];
    }

    ngOnInit() {
        this.service.tree();
        this.store
            .select(state => state.category.tree)
            .subscribe(value => {
                this.items = convert(value)
                    .map(value2 => {
                        return new TreeviewItem(value2);
                    });
            });
    }

}
