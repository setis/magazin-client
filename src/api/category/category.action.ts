import {Action} from '@ngrx/store';
import {Category, CategoryTree} from './category.models';

export const CATEGORY_ITEMS_LOAD = '[Category] Items Load';
export const CATEGORY_TREE_LOAD = '[Category] Tree Load';
export const CATEGORY_CREATE = '[Category] Create';
export const CATEGORY_CREATE_SUCCESS = '[Category] Create Success';


export class CategoryItemsLoadAction implements Action {
    type = CATEGORY_ITEMS_LOAD;

    constructor(public payload: Category[]) {
    }
}

export class CategoryTreeLoadAction implements Action {
    type = CATEGORY_TREE_LOAD;

    constructor(public payload: CategoryTree[]) {
    }
}

export class CategoryCreateAction implements Action {
    type = CATEGORY_CREATE;

    constructor(public payload: {
        parent: number;
        name: string;
        description: string | null;
    }) {
    }
}
export class CategoryCreateSuccessAction implements Action {
    type = CATEGORY_CREATE_SUCCESS;

    constructor(public payload: Category) {
    }
}

export type Actions = CategoryItemsLoadAction | CategoryTreeLoadAction | CategoryCreateSuccessAction;
