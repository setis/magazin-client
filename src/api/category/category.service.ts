import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Category, CategoryTree} from './category.models';
import {Store} from '@ngrx/store';
import {State} from '../api.reducer';
import {LoadingState} from '../loading/loading.reducer';
import 'rxjs/add/operator/filter';
import {LoadingFailAction, LoadingSendAction, LoadingSuccessAction} from '../loading/loading.action';
import {CategoryCreateSuccessAction, CategoryItemsLoadAction, CategoryTreeLoadAction} from './category.action';


@Injectable()
export class CategoryService {
    private api: string;
    private loading: LoadingState;

    constructor(private http: HttpClient, private store: Store<State>) {
        this.store
            .select(state => state.configure.server.full)
            .subscribe(value => {
                this.api = value;
            });
        this.store
            .select(state => state.loading)
            .map(value => {
                let result = {};
                Object.keys(value)
                    .forEach(value2 => {
                        if (/^category/gi.test(value2)) {
                            result[value2] = value[value2];
                        }
                    });
                return result;
            })
            .subscribe(value => {
                this.loading = value;
            });


    }

    public list() {
        let event = 'category.list';
        if (this.loading[event] && this.loading[event].loading === true) {
            return;
        }
        this.store.dispatch(new LoadingSendAction(event));
        this.http
            .get<{
                status: 'success' | 'error',
                success?: Category[],
                error?: {
                    msg: string,
                    stack: string,
                    code: string
                }
            }>(`${this.api}/category/list`, {observe: 'response'})
            .subscribe(value => {
                this.store.dispatch(new LoadingSuccessAction(event));
                this.store.dispatch(new CategoryItemsLoadAction(value.body.success));
            }, (err) => {
                this.store.dispatch(new LoadingFailAction(event, err));
            });
    }

    public tree() {
        let event = 'category.tree';
        if (this.loading[event] && this.loading[event].loading === true) {
            return;
        }
        this.store.dispatch(new LoadingSendAction(event));
        this.http
            .get<{
                status: 'success' | 'error',
                success?: CategoryTree[],
                error?: {
                    msg: string,
                    stack: string,
                    code: string
                }
            }>(`${this.api}/category/tree`)
            .subscribe(value => {
                this.store.dispatch(new LoadingSuccessAction(event));
                this.store.dispatch(new CategoryTreeLoadAction(value.success));
            }, (err) => {
                this.store.dispatch(new LoadingFailAction(event, err));
            });

    }

    public create(parameters: { parent: number, name: string, description?: string }) {
        let event = 'category.create';
        if (this.loading[event] && this.loading[event].loading === true) {
            return;
        }
        this.store.dispatch(new LoadingSendAction(event));
        this.http
            .post<{
                status: 'success' | 'error',
                success?: Category,
                error?: {
                    msg: string,
                    stack: string,
                    code: string
                }
            }>(`${this.api}/category/create`, parameters)
            .subscribe(
                value => {
                    this.store.dispatch(new LoadingSuccessAction(event));
                    this.store.dispatch(new CategoryCreateSuccessAction(value.success));
                }, (err: HttpErrorResponse) => {
                    this.store.dispatch(new LoadingFailAction(event, err));
                });
    }


}
