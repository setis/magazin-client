import {Directive, Input} from '@angular/core';
import {AbstractControl, ValidationErrors, Validator, ValidatorFn} from '@angular/forms';
import {Store} from '@ngrx/store';
import {State} from '../api.reducer';

export function CategoryUniqNameValidator(names: string[]): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        return (names.indexOf(control.value) !== -1) ? {'name': {value: control.value}} : null;
    };
}

@Directive({
    selector: '[appCategoryUniqName]'
})
export class CategoryUniqNameDirective implements Validator {
    @Input('appCategoryUniqName')
    name: string;
    items: string[];

    constructor(private store: Store<State>) {
        this.store.select(state => state.category.items).subscribe(value => {
            this.items = value.map(value2 => value2.name);
        });
    }

    validate(c: AbstractControl): ValidationErrors | null {
        return this.name ? CategoryUniqNameValidator(this.items)(c) : null;
    }

}
