export interface Category {
    id: number;
    name: string;
    description: string | null;
    level: number;
}

export interface CategoryTree extends Category {

    children: CategoryTree[];

}
