import {Component, OnInit} from '@angular/core';
import {CategoryService} from './category.service';

@Component({
    selector: 'category-root',
    templateUrl: './category.component.html',
    providers: [
        CategoryService
    ]
})
export class CategoryComponent implements OnInit {
    constructor(private category: CategoryService) {
    }

    ngOnInit(): void {
        console.log('asdasd');
        this.category.list();
        this.category.tree();
    }
}
