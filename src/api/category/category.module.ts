import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CategoryCreateComponent} from './create/create.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {CategoryService} from './category.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CategoryUniqNameDirective} from './uniq.name.directive';
import {CategorySelectComponent} from './select/select.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CategoryTreeComponent} from './tree/tree.component';
import {TreeviewModule} from 'ngx-treeview/src/treeview.module';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {reducer} from '../api.reducer';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {CategoryComponent} from './category.component';

const routes: Routes = [
    {path: 'create', component: CategoryCreateComponent},
    {path: 'select', component: CategorySelectComponent},
    {path: 'tree', component: CategoryTreeComponent},
];

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        TreeviewModule,
        // RouterModule.forChild(routes)

    ],
    declarations: [
        CategoryCreateComponent,
        CategoryUniqNameDirective,
        CategorySelectComponent,
        CategoryTreeComponent,
        CategoryComponent
    ],
    exports: [
        RouterModule
    ],
    providers: [
        CategoryService
    ]
})
export class CategoryModule {
}

