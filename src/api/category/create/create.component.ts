import {Component, Input, OnInit} from '@angular/core';
import {CategoryService} from '../category.service';
import {Location} from '@angular/common';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CategoryUniqNameValidator} from '../uniq.name.directive';
import {Category} from '../category.models';
import {Store} from '@ngrx/store';
import {State} from '../../api.reducer';
import {LoadingState} from '../../loading/loading.reducer';

@Component({
    selector: 'app-category-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss'],

})
export class CategoryCreateComponent implements OnInit {

    options: Category[];
    form: FormGroup;
    @Input()
    model: {
        name: string,
        description?: string | null,
        parent: {
            id: number,
            name: string
        }
    };
    error: boolean;
    loading: boolean;
    status: 'none' | 'success' | 'error';
    id: number | null;
    @Input()
    timeout: number;
    names: string[];

    constructor(private store: Store<State>, private service: CategoryService, private location: Location) {
        this.model = {
            name: '',
            description: null,
            parent: {
                id: 0,
                name: '/',
            }
        };
        this.timeout = 5;
        this.error = false;
        this.loading = false;
        this.status = 'none';
        this.names = [];
    }

    get name() {
        return this.form.get('name');
    }

    ngOnInit() {
        this.form = new FormGroup({
            name: new FormControl(this.model.name, [
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(255),
                CategoryUniqNameValidator(this.names)
            ]),
            description: new FormControl(this.model.description),
            parent: new FormControl(this.model.parent),

        });
        this.service.list();
        this.store
            .select(state => state.category.items)
            .subscribe(value => {
                this.options = value;
                this.names = value.map(value2 => value2.name);
                this.form.controls.name.setValidators([
                    Validators.required,
                    Validators.minLength(1),
                    Validators.maxLength(255),
                    CategoryUniqNameValidator(this.names)
                ]);
            });
        this.store
            .select<LoadingState>(state => state.loading)
            .subscribe(value => {
                let chunk = value['category.create'];
                if (chunk === undefined) {
                    this.loading = false;
                    this.error = false;
                    this.status = 'none';
                } else {
                    this.loading = chunk.loading;
                    if (this.loading === false) {
                        if (chunk.fail !== false) {
                            this.error = chunk.fail.error.error;
                            this.status = 'error';
                        } else {
                            this.error = false;
                            this.status = 'success';
                        }
                    }
                }
            });

        this.form
            .valueChanges
            .subscribe(value => {
                this.model = value;
            });

    }

    onSubmit() {
        this.loading = true;
        this.error = false;
        this.status = 'none';
        return this.service
            .create({
                name: this.model.name,
                description: this.model.description,
                parent: this.model.parent.id,

            });
    }

    onReset() {
        this.loading = false;
        this.error = false;
        this.status = 'none';
        this.clearTimer();
        this.form.reset({
            name: '',
            description: null,
            parent: {
                id: 0,
                name: '/',
            }
        });
    }

    onCancel() {
        this.location.back();
    }

    protected timer() {
        if (this.id !== null) {
            clearTimeout(this.id);
            this.id = null;
        }
        this.id = setTimeout(() => {
            this.status = 'none';
        }, this.timeout * 1e3);
    }

    protected clearTimer() {
        if (this.id !== null) {
            clearTimeout(this.id);
            this.id = null;
        }
    }

}
