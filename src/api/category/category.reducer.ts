import {Category, CategoryTree} from './category.models';
import {Actions, CATEGORY_ITEMS_LOAD, CATEGORY_TREE_LOAD, CategoryItemsLoadAction, CategoryTreeLoadAction} from './category.action';

export interface CategoryState {
    items: Category[];
    tree: CategoryTree[];
    select: 'list' | 'search';
    search: {
        parent?: number;
        level?: number;
        remote: boolean;
        size: number;
        // list: Category[];
        // query: string;
    };
    wizard: {
        items: (Category | number)[];
        step: number;
        complete: boolean;
    };
}

const initialState: CategoryState = {
    items: [],
    tree: [],
    select: 'search',
    search: {
        remote: false,
        size: 10,
    },
    wizard: {
        items: [],
        step: 0,
        complete: false
    }
};

export function reducer(state = initialState, action: Actions): CategoryState {
    switch (action.type) {
        case CATEGORY_ITEMS_LOAD: {
            if (!(action instanceof CategoryItemsLoadAction)) {
                throw new Error();
            }
            return {...state, items: action.payload};
        }
        case CATEGORY_TREE_LOAD: {
            if (!(action instanceof CategoryTreeLoadAction)) {
                throw new Error();
            }
            return {...state, tree: action.payload};
        }
        default: {
            return state;
        }
    }
}
