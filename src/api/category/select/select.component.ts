import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {CategoryService} from '../category.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {Store} from '@ngrx/store';
import {State} from '../../api.reducer';
import {Category} from '../category.models';

function escape(string) {
    return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

@Component({
    selector: 'app-category-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [CategoryService],
})
export class CategorySelectComponent implements OnInit {
    @Input()
    msg = {
        'loading': 'идет поиск',
        'label': 'выберите категорию'
    };
    @Input() model: Category;
    @Input() count: number;
    states: Category[];
    loading: boolean;
    error: false | { msg: string, code?: number | string, stack: string };

    constructor(private store: Store<State>, private service: CategoryService) {
        this.search = this.search.bind(this);
        this.error = false;
        this.loading = false;
        this.count = 10;
        this.states = [];


    }

    formatter(x: Category): string {
        return x.name;
    }

    search(text$: Observable<string>) {

        return text$
            .debounceTime(300)
            .distinctUntilChanged()
            .map(term => {
                    return term === '' ? []
                        : (() => {
                            let reg = escape(term);
                            return this.states
                                .filter(v => {
                                    return (new RegExp(reg, 'gi')).test(v.name);
                                });
                        })();
                }
            )
            .take(this.count);
    }

    ngOnInit() {
        this.service.list();
        this.store
            .select(state => state.category.items)
            .subscribe(value => {
                this.states = value;
            });
    }


}
