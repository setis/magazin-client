import {Action} from '@ngrx/store';
import {HttpErrorResponse} from '@angular/common/http';

export const LOADING_SEND = '[Loading] Send';
export const LOADING_SUCCESS = '[Loading] Success';
export const LOADING_FAIL = '[Loading] Fail';

export class LoadingSendAction implements Action {
    readonly type = LOADING_SEND;

    constructor(public payload: string) {
    }
}

export class LoadingSuccessAction implements Action {
    readonly type = LOADING_SUCCESS;

    constructor(public payload: string) {
    }
}

export class LoadingFailAction implements Action {
    readonly type = LOADING_FAIL;

    constructor(public name: string, public payload: HttpErrorResponse) {
    }
}


export type Actions = LoadingSendAction | LoadingSuccessAction | LoadingFailAction;
