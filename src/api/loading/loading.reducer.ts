import {HttpErrorResponse} from '@angular/common/http';
import {
    Actions,
    LOADING_FAIL,
    LOADING_SEND,
    LOADING_SUCCESS,
    LoadingFailAction,
    LoadingSendAction,
    LoadingSuccessAction
} from './loading.action';

export interface LoadingStateChunk {
    try: number;
    loading: boolean;
    fail: false | HttpErrorResponse
    time: Date;
    loaded: boolean;

}

export interface LoadingState {
    [name: string]: LoadingStateChunk;
}

const initialState: LoadingState = {};

export function reducer(state: LoadingState = initialState, action: Actions): LoadingState {
    switch (action.type) {
        case LOADING_SEND: {
            if (!(action instanceof LoadingSendAction)) {
                throw new Error();
            }
            let chunk = state[action.payload];
            if (chunk === undefined) {
                state[action.payload] = {
                    try: 0,
                    loading: true,
                    fail: false,
                    time: new Date(),
                    loaded: false
                };
                return {...state};
            } else {
                chunk.loading = true;
                chunk.time = new Date();
                return {...state};
            }
        }
        case LOADING_SUCCESS: {
            if (!(action instanceof LoadingSuccessAction)) {
                throw new Error();
            }
            let chunk = state[action.payload];
            if (chunk === undefined) {
                state[action.payload] = {
                    try: 0,
                    loading: false,
                    fail: false,
                    time: new Date(),
                    loaded: true
                };
            } else {
                let result = {
                    try: 0,
                    loading: false,
                    fail: false,
                    loaded: true
                };
                Object.keys(result).forEach(value => {
                    chunk[value] = result[value];
                });
            }

            return {...state};
        }
        case LOADING_FAIL: {
            if (!(action instanceof LoadingFailAction)) {
                throw new Error();
            }
            let chunk = state[action.name];
            if (chunk === undefined) {
                state[action.name] = {
                    try: 1,
                    loading: false,
                    fail: action.payload,
                    time: new Date(),
                    loaded: false
                };
            } else {
                chunk.loading = false;
                chunk.fail = action.payload;
                chunk.try++;
            }

            return {...state};
        }
        default: {
            return state;
        }
    }
}
