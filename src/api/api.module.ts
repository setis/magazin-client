import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {CategoryModule} from './category/category.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TreeviewModule} from 'ngx-treeview';
import {RouterModule, Routes} from '@angular/router';
import {StoreModule} from '@ngrx/store';
import {reducers} from './api.reducer';
import {ApiComponent} from './api.component';
import {ConfigureService} from './configure/configure.service';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {CategoryTreeComponent} from './category/tree/tree.component';
import {CategorySelectComponent} from './category/select/select.component';
import {CategoryCreateComponent} from './category/create/create.component';
import {CategoryUniqNameDirective} from './category/uniq.name.directive';
import {CategoryComponent} from './category/category.component';
import {CategoryService} from './category/category.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
    // {path: 'category', loadChildren: './category/category.module#CategoryModule'},

    // {path: 'not-found', loadChildren: './category/category.module#CategoryModule'},
    // {path: '**', redirectTo: 'category/select'},

    {path: 'category/create', component: CategoryCreateComponent},
    {path: 'category/select', component: CategorySelectComponent},
    {path: 'category/tree', component: CategoryTreeComponent},
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        NgbModule.forRoot(),
        TreeviewModule.forRoot(),
        ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),

        /**
         * StoreModule.provideStore is imported once in the root module, accepting a reducer
         * function or object map of reducer functions. If passed an object of
         * reducers, combineReducers will be run creating your application
         * meta-reducer. This returns all providers for an @ngrx/store
         * based application.
         */
        StoreModule.forRoot(reducers),

        RouterModule.forRoot(routes, {enableTracing: true}),
        /**
         * @ngrx/router-store keeps router state up-to-date in the store and uses
         * the store as the single source of truth for the router's state.
         */
        // StoreRouterConnectingModule,
        /**
         * Store devtools instrument the store retaining past versions of state
         * and recalculating new states. This enables powerful time-travel
         * debugging.
         *
         * To use the debugger, install the Redux Devtools extension for either
         * Chrome or Firefox
         *
         * See: https://github.com/zalmoxisus/redux-devtools-extension
         */
        // StoreDevtoolsModule.instrument(),

        /**
         * EffectsModule.run() sets up the effects class to be initialized
         * immediately when the application starts.
         *
         * See: https://github.com/ngrx/effects/blob/master/docs/api.md#run
         */
        // EffectsModule.run(BookEffects),
        // EffectsModule.run(CollectionEffects),

        /**
         * `provideDB` sets up @ngrx/db with the provided schema and makes the Database
         * service available.
         */
        // DBModule.provideDB(schema),
        // CategoryModule,
        FormsModule,
        ReactiveFormsModule,
        // NgbModule,
        // TreeviewModule,

    ],
    providers: [
        ConfigureService,

        CategoryService
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        ApiComponent,

        CategoryCreateComponent,
        CategoryUniqNameDirective,
        CategorySelectComponent,
        CategoryTreeComponent,
        CategoryComponent
    ],
    bootstrap: [
        ApiComponent
    ]
})
export class ApiModule {
}
